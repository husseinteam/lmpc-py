import sys
import os

def importkit(package, module):
  impstr = "{0}.{1}".format(package, module)
  rm = None
  try:
    rm = __import__(impstr, fromlist=[""])
  except:
    print("import not successful for:\n\t{0}".format(impstr))
    sys.exit();
  return rm;

if __name__ == "__main__":
  kit = importkit("out", "notifiers")
  kit.printf("Hello {0}", "world!")
  kit = importkit("binaryops", "bit")
  print(kit.bit(100))
  input("hit to exit..")
