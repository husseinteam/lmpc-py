def _bit(n):
  if n == 0: return ""
  return "" + str(n % 2) + _bit(n // 2)

def bit(n):
  return(_bit(n)[::-1])

if __name__ == "__main__":
  print(bit(100))
