
import sys
import time

def _getValueName(var, lcls, glbls=None):
  for k, v in lcls.items():
    if type(v) == type(var):
      if v == var:
        return k
  if not glbls: return None
  for k, v in glbls.items():
    if type(v) == type(var):
      if v == var:
        return k
  return None
getvn = _getValueName
messages = {
  "TypesMismatch" : lambda var, musttype: "<{0}><{1}> not of type <{2}>"\
    .format(var, type(var), musttype),
  "KeyNotFound" : lambda k, d: "key<{0}> not found in dict.keys<{1}>"\
    .format(k, d.keys()),
  "ValuesMismatch" : lambda var, must, lcls: "{0}<{2}> != {1}<{3}>"\
    .format(getvn(var, lcls), getvn(must, lcls), var, must),
  "InvalidValue" : lambda var, musnt, lcls: "Invalid Value! {0}<{1}> cannot be {2}"\
    .format(getvn(var, lcls), var, musnt),
  "Overflow" : lambda var, limit, lcls: "{0}<{1}> must be lte then {2}"\
    .format(getvn(var, lcls), var, limit),
  }

def exitf(msg, *args):
  printf(msg, *args)
  input("press enter to exit.")

def printf(msg, *args):
  if len(args) == 0:
    print(msg)
  else:
    print(str(msg).format(*args))

def printnsleep(msg, *args, sleepfor=1):
  printf(msg, *args)
  time.sleep(sleepfor)

def assertf(flag, msg="", *args):
  if not flag:
    printf("assertion error!\n" + msg, *args)
    assert(False)
  
def asserts(flag, msgkey, *args):
  if not flag:
    asserts(msgkey in messages.keys(), "KeyNotFound", msgkey, messages)
    printf(messages[msgkey](*args))
    assert(False)

def dump(varstr, lcls={}, glbls={}):
  def parsetypename(v):
    import re
    pttrn = re.compile(r"^<.*('|\.)(.+)'>$")
    #print(pttrn.search("<class '__main__.TMember.Main'>").groups())
    #print(pttrn.search("<class 'int'>").groups())
    m = pttrn.match(str(type(v)))
    return m.groups()[-1] if m is not None else "None"
  msg = "dump of {0}:\n\t".format(varstr)
  for svar in varstr.split(";"):
    if svar in lcls.keys():
      var = lcls[svar]
    elif svar in glbls.keys():
      var = glbls[svar]
    else:
      var = None
    msg += "dump of: {0}<{2}> = {1}\n\t"\
           .format(svar, var, parsetypename(var))
  printf(msg)


    
