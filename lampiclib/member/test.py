
from MMember import *
import random as rnd

def getrandom():
  magic1 = rnd.random() * 999999999999 // 11
  magic2 = rnd.random() * 999999999999 // 19
  magic1 = int(magic1*magic1)
  magic2 = int(magic2*magic2)

  m1 = TMember.FromInt(magic1, base=10)
  m2 = TMember.FromInt(magic2, base=10)
  return (m1, m2) if m1 >= m2 else (m2, m1)

for i in range(10):
  for sign in [">", "<", ">=", "<=", "+", "-", "*", "//"]:
    m1, m2 = getrandom()
    real = "{1} {0} {2}".format(sign, m1, m2)
    must = "m1 {0} m2".format(sign)
    realeval = eval(real, locals())
    musteval = eval(must, locals())
    printf("expression:{0}\n\tEvaluated as {1} must be {2}"\
           , must, realeval, musteval)
    if realeval == musteval:
      printf("evaluation successful")
    else:
      printnsleep("evaluation failed.", sleepfor=2)
      exitf("exiting..")
printnsleep("remember member :)", sleepfor=7)
