from arc import Arc

class Unit(object):
  Sealed = False;
  
  def __init__(self, arcCount):
    assert(not Unit.Sealed);
    self.ArcCount = arcCount;
    self._ConstructUnit();
    Unit.Sealed = True;

  def _ConstructUnit(self):
    self.Chain = [];
    for i in range(self.ArcCount):
      self.Chain.append(Arc());
    for i in range(1, self.ArcCount):
      self.Chain[i - 1].ArcTo(self.Chain[i]);
    self.Chain[-1].ArcTo(self.Chain[0]);

  def BreakChain(self, i):
    assert(i < self.ArcCount);
    self.Chain[i - 1].Former = None;
    self.Chain[i].Lesser = None;
  
  def BranchAt(self, i, n):
    assert(i < self.ArcCount);
    Unit.Sealed = False;
    u = Unit(n);
    self.BreakChain(i);
    u.BreakChain(0);
    self.Chain[i - 1].ArcTo(u.Chain[0]);
    u.Chain[-1].ArcTo(self.Chain[i].Former);

  def Iterate(self):
    rootArc = self.Chain[0];
    yield rootArc;
    nextArc = rootArc.Former;
    while not nextArc.Former.Equals(rootArc):
      nextArc = nextArc.Former;
      yield nextArc;
    yield nextArc;

if __name__ == "__main__":
  u = Unit(20);
  u.BranchAt(12, 66);
  for i, arc in enumerate(u.Iterate()):
    print(arc.Guid, i);
