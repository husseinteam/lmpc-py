
class Handlers(object):
  def __init__(self):
    self._HDict = {};

  def InsertHandler(self, key, h):
    assert(hasattr(h, "__call__"));
    self._HDict[key] = h

  def HandleByKey(self, key):
    assert(self._HDict[key]);
    return self._HDict[key];
