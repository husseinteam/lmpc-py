
import re;
import urllib.request as ur;
import os;

opj = os.path.join;
cwd = os.getcwd;
DOWNDIR = opj(cwd(), "down");

class HtmlExtractor(object):
  def __init__(self, url):
    self._InitMarkUp(url);
    self._FilterLinks();

  def _InitMarkUp(self, url):
    self.Url = url;
    self.MarkUp = ""; 
    try:
      self.MarkUp = ur.urlopen(self.Url).read().decode('latin5');
    except Exception as e:
      try:
        self.MarkUp = str(open(url).read())
      except Exception as e2:
        print(e)
        import sys
        sys.exit(1)
    #self.MarkUp at hand.
    
  def _FilterLinks(self):
    self.Links = [];
    #<a href="/" class="btn1" title="ANA SAYFA">ANA SAYFA</a>
    ptrnLink = "<a\s+href={0}(.*?){0}";
    for it in re.finditer(ptrnLink.format("'"), self.MarkUp):
      self.Links.append(it.group(1));
    for it in re.finditer(ptrnLink.format("\""), self.MarkUp):
      self.Links.append(it.group(1));

  def DumpLinks(self):
    for it in iter(self.Links):
      print("found link at: %s" %it);
  
  def DownloadLinks(self):
    for l in iter(self.Links):
      fn = ur.urlsplit(l)[2].replace("/", "");
      try:
        ur.urlretrieve(l, opj(DOWNDIR, fn));
      except Exception as e:
        print("Link could not be downloaded: {0}\n".format(l))
      else:
        print("Link downloaded: {0}\n".format(l))
      
