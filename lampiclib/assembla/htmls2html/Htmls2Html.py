import os
from ExtractHtml import *

class Htmls:
    def toHtml(self, srcpath, ext):
        import glob
        pathlist = glob.glob(os.path.join(srcpath, '*.html'))
        pathlist = [p for p in pathlist if not p == os.path.join(srcpath, 'default.html')]
        out = open(os.path.join(srcpath, 'default.html'), 'w')
        out.write('<html> /n')
        for path in pathlist:
            f = open(path)
            if ext.do(f):
                out.write(ext.textout)
            else:
                print 'No match found for ' + path
            f.close()
            ext.reset()
            print path + ' completed..'
        out.write('</html> /n')
        out.close()
        
if __name__ == '__main__':
    e = Extractor('body')
    htmls = Htmls()
    srcpath = '0131429388'
    htmls.toHtml(srcpath, e)
    
