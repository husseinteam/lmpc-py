import os

class Extractor:
    def __init__(self, surrtag):
        self.surrtag = surrtag
        self.textout = ''

    def do(self, htmlfile):
        htmltxt = htmlfile.read()
        #ltagchars = list(surrtag)
        htmllen = len(htmltxt)
        taglen = len(self.surrtag)
        limit = htmllen-taglen-2
        i = 0; j = 0
        endindex = 0; startindex = 0
        #start tag scan starts
        for i in range(0,limit):
            if (htmltxt[i] == '<') & (htmltxt[i+taglen+1] == '>'):
                if htmltxt[i+1:i+taglen+1] == self.surrtag:
                    #starttag found!
                    startindex = i + taglen + 2
                    #2 for '<>'
                    break
        #now for end tag scan starts
        while i<limit:
            if (htmltxt[i:i+2] == '</') & (htmltxt[i+taglen+2] == '>'):
                if htmltxt[i+2:i+taglen+2] == self.surrtag:
                    #endtag found!
                    endindex = i
                    #index of char after last
                    break
            i += 1        
                
        if i== limit:
            return False
        else:
            self.textout = str(htmltxt[startindex:endindex])
            return True

    def reset(self):
        self.textout = ''
        
if __name__ == '__main__':
    e = Extractor('body')
    htmlin = open('w3.html')
    if e.do(htmlin):
        f = open('out.html', 'w')
        f.write('''
                    <html><head><title>This is a try out stripped html</title></head>
                    <body>
                ''')
        f.write(e.textout)
        f.write('''
                    </body>
                ''')
        f.close()
        e.reset()
        print('success')
    else:
        print('No match found!!')
