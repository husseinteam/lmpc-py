'''
Created on 13.May.2009

@author: Ozgur.Sonmez
'''

class AttrDataStore(object):
    '''
    Structure for Attributes of the Tags
    '''
    def __init__(self):
        self.d = \
        {
         'attrs':[],
         'values':[],
         'descriptions':[]
        }
        self.pushDict()
    
    def pushDict(self):
        '''
        Pushes self.attrList
        '''
        def filtfunc(attr):
            if (attr[:3]=='__') or (attr[-2:]=='__'):
                return False
            else:
                return True
        genCls = (getattr(self, cls) for cls in dir(self) \
                   if isinstance(cls, object))
        for cls in genCls:
            attrList = [getattr(cls, attr) for attr in dir(cls) \
                        if filtfunc(attr)]
            for attr in attrList:
                attr = self.d.copy()
                
    class basic:
        _html = \
        _body = \
        _h1 = \
        _h2 = \
        _h3 = \
        _h4 = \
        _h5 = \
        _h6 = \
        _p = \
        _br = \
        _hr = \
        _comment = \
        None
        
    class char:
        _b = \
        _font = \
        _i = \
        _em = \
        _big = \
        _strong = \
        _small = \
        _sup = \
        _sub = \
        _bdo = \
        _u = \
        None
        
    class output:
        _pre = \
        _code = \
        _tt = \
        _kbd = \
        _var = \
        _dfn = \
        _samp = \
        None
    
    class blocks:
        _acronym = \
        _abbr = \
        _address = \
        _blockquote = \
        _center = \
        _q = \
        _cite = \
        _ins = \
        _del = \
        _s = \
        _strike = \
        None
    
    class links:
        _link = \
        _a = \
        None
    
    class frames:         
        _frame = \
        _frameset = \
        _noframes = \
        _iframe = \
        None
    
    class input:           
        _form = \
        _input = \
        _textarea = \
        _button = \
        _select = \
        _optgroup = \
        _option = \
        _label = \
        _fieldset = \
        _legend = \
        None
        
    class lists:          
        _ul = \
        _ol = \
        _li = \
        _dl = \
        _dt = \
        _dd = \
        None
    
    class images:          
        _img = \
        _map = \
        _area = \
        None
    
    class tables:           
        _table = \
        _caption = \
        _th = \
        _tr = \
        _td = \
        _thead = \
        _tbody = \
        _tfoot = \
        _col = \
        _colgroup = \
        None
    
    class styles:         
        _style = \
        _div = \
        _span = \
        None
    
    class metainfo:          
        _head = \
        _title = \
        _meta = \
        _base = \
        _basefont = \
        None
    
    class programming:          
        _script = \
        _noscript = \
        _applet = \
        _object = \
        _param = \
        None
        
if __name__ == "__main__":
    try:
        adt = AttrDataStore()
    except Exception as e:
        print ("Error!:\n", e)
    else:
        print ("Success in", "AttrDataStore")
        
