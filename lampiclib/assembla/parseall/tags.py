'''
Created on 13.May.2009

@author: Ozgur.Sonmez
'''
  
from tagdatastore import *            

class Tags():
    '''
    Class for List of Tags
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self.initComponents()
    
    def initComponents(self):
        '''
        Initializes Components
        '''
        self.datastore = TagDataStore()
        
if __name__ == "__main__":
    try:
        attrds = AttrDataStore()
        tagds = TagDataStore()
        t = Tags()
    except Exception as e:
        print ("Object Creation Not Successful\nError:", e)
        import sys
        sys.exit()
    else:
        print ("Object Creation Successful in", "tag.py")

#    for attr in da.basicbody.keys():
#        print attr
#    da.basicbody['values'].append('basicbody')
#    da.basichtml['values'].append('basichtml')
#    print "da.basicbody['values']", da.basicbody['values']
#    print "da.basichtml['values']", da.basichtml['values']    
