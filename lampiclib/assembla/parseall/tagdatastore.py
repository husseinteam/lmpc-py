'''
Created on 13.May.2009

@author: Ozgur.Sonmez
'''

from attrdatastore import * 
attrs = AttrDataStore()

class TagDataStore:
    '''
    Structure for Tags
    '''
    basic =         [{'name':'!DOCTYPE', 
                      'attrs':{}}, 
                     {'name':'html', 
                      'attrs':attrs.basic._html},
                     {'name':'body', 
                      'attrs':attrs.basic._body},
                     {'name':'h1', 
                      'attrs':attrs.basic._h1},
                     {'name':'h2', 
                      'attrs':attrs.basic._h2},
                     {'name':'h3', 
                      'attrs':attrs.basic._h3},
                     {'name':'h4', 
                      'attrs':attrs.basic._h4},
                     {'name':'h5', 
                      'attrs':attrs.basic._h5},
                     {'name':'h6', 
                      'attrs':attrs.basic._h6},
                     {'name':'p', 
                      'attrs':attrs.basic._p},
                     {'name':'br', 
                      'attrs':attrs.basic._br},
                     {'name':'hr', 
                      'attrs':attrs.basic._hr}
                    ]
    char =          [{'name':'b', 
                      'attrs':attrs.char._b},
                     {'name':'bdo', 
                      'attrs':attrs.char._bdo},
                     {'name':'big', 
                      'attrs':attrs.char._big},
                     {'name':'em', 
                      'attrs':attrs.char._em},
                     {'name':'font', 
                      'attrs':attrs.char._font},
                     {'name':'i', 
                      'attrs':attrs.char._i},
                     {'name':'small', 
                      'attrs':attrs.char._small},
                     {'name':'strong', 
                      'attrs':attrs.char._strong},
                     {'name':'sub', 
                      'attrs':attrs.char._sub},
                     {'name':'sup', 
                      'attrs':attrs.char._sup},
                     {'name':'u', 
                      'attrs':attrs.char._u}
                    ] 
    output =        [{'name':'code', 
                      'attrs':attrs.output._code},
                     {'name':'dfn', 
                      'attrs':attrs.output._dfn},
                     {'name':'kbd', 
                      'attrs':attrs.output._kbd},
                     {'name':'pre', 
                      'attrs':attrs.output._pre},
                     {'name':'samp', 
                      'attrs':attrs.output._samp},
                     {'name':'tt', 
                      'attrs':attrs.output._tt},
                     {'name':'var', 
                      'attrs':attrs.output._var}
                     ] 
    blocks =        [{'name':'abbr', 
                      'attrs':attrs.blocks._abbr},
                     {'name':'acronym', 
                      'attrs':attrs.blocks._acronym},
                     {'name':'address', 
                      'attrs':attrs.blocks._address},
                     {'name':'blockquote', 
                      'attrs':attrs.blocks._blockquote},
                     {'name':'center', 
                      'attrs':attrs.blocks._center},
                     {'name':'cite', 
                      'attrs':attrs.blocks._cite},
                     {'name':'del', 
                      'attrs':attrs.blocks._del},
                     {'name':'ins', 
                      'attrs':attrs.blocks._ins},
                     {'name':'q', 
                      'attrs':attrs.blocks._q},
                     {'name':'s', 
                      'attrs':attrs.blocks._s},
                     {'name':'strike', 
                      'attrs':attrs.blocks._strike}
                    ] 
    links =         [{'name':'a', 
                      'attrs':attrs.links._a},
                     {'name':'link', 
                      'attrs':attrs.links._link}
                     ]
    frames =        [{'name':'frame', 
                      'attrs':attrs.frames._frame},
                     {'name':'frameset', 
                      'attrs':attrs.frames._frameset},
                     {'name':'iframe', 
                      'attrs':attrs.frames._iframe},
                     {'name':'noframes', 
                      'attrs':attrs.frames._noframes}
                     ]
    input =         [{'name':'form', 
                      'attrs':attrs.input._form},
                     {'name':'input', 
                      'attrs':attrs.input._input},
                     {'name':'textarea', 
                      'attrs':attrs.input._textarea},
                     {'name':'button', 
                      'attrs':attrs.input._button},
                     {'name':'select', 
                      'attrs':attrs.input._select},
                     {'name':'optgroup', 
                      'attrs':attrs.input._optgroup},
                     {'name':'option', 
                      'attrs':attrs.input._option},
                     {'name':'label', 
                      'attrs':attrs.input._label},
                     {'name':'fieldset', 
                      'attrs':attrs.input._fieldset},
                     {'name':'legend', 
                      'attrs':attrs.input._legend}
                     ]
    lists =         [{'name':'ul', 
                      'attrs':attrs.lists._ul},
                     {'name':'ol', 
                      'attrs':attrs.lists._ol},
                     {'name':'li', 
                      'attrs':attrs.lists._li},
                     {'name':'dl', 
                      'attrs':attrs.lists._dl},
                     {'name':'dt', 
                      'attrs':attrs.lists._dt},
                     {'name':'dd', 
                      'attrs':attrs.lists._dd}
                     ]
    
    images =        [{'name':'img', 
                      'attrs':attrs.images._img},
                     {'name':'map', 
                      'attrs':attrs.images._map},
                     {'name':'area', 
                      'attrs':attrs.images._area}
                     ]
    tables =        [{'name':'table', 
                      'attrs':attrs.tables._table},
                     {'name':'caption', 
                      'attrs':attrs.tables._caption},
                     {'name':'th', 
                      'attrs':attrs.tables._th},
                     {'name':'tr', 
                      'attrs':attrs.tables._tr},
                     {'name':'td', 
                      'attrs':attrs.tables._td},
                     {'name':'thead', 
                      'attrs':attrs.tables._thead},
                     {'name':'tbody', 
                      'attrs':attrs.tables._tbody},
                     {'name':'tfoot', 
                      'attrs':attrs.tables._tfoot},
                     {'name':'col', 
                      'attrs':attrs.tables._col},
                     {'name':'colgroup', 
                      'attrs':attrs.tables._colgroup}
                     ]
    styles =        [{'name':'style', 
                      'attrs':attrs.styles._style},
                     {'name':'div', 
                      'attrs':attrs.styles._div},
                     {'name':'span', 
                      'attrs':attrs.styles._span}
                     ]
    metainfo =      [{'name':'head', 
                      'attrs':attrs.metainfo._head},
                     {'name':'title', 
                      'attrs':attrs.metainfo._title},
                     {'name':'meta', 
                      'attrs':attrs.metainfo._meta},
                      {'name':'base', 
                      'attrs':attrs.metainfo._base},
                      {'name':'basefont', 
                      'attrs':attrs.metainfo._basefont}
                     ]
    programming =   [{'name':'head', 
                      'attrs':attrs.programming._script},
                     {'name':'title', 
                      'attrs':attrs.programming._noscript},
                     {'name':'meta', 
                      'attrs':attrs.programming._applet},
                      {'name':'base', 
                      'attrs':attrs.programming._object},
                      {'name':'basefont', 
                      'attrs':attrs.programming._param}
                     ] 


if __name__ == "__main__":
    try:
        tds = TagDataStore()
    except Exception as e:
        print ("Error!:\n", e)
    else:
        print ("Success in", "TagDataStore")
