'''
Created on 13.May.2009

@author: Ozgur.Sonmez
'''

class Root():
    '''
    Root Class for Html Parsing
    '''
    def __init__(self, html):
        self.initComponents()
        self.pushhtml(html)
    def initComponents(self):
        '''Starts class components'''
        self.html = None
            #html file descriptor or url file descriptor
        self.hypertext = ''
            #hypertext string the html file or url gave
    def pushhtml(self, html):
        '''pushes self.html'''
        isFile = False
        try:
            self.html = urllib.urlopen(html)
        except IOError as e:
            print ("url can not be opened", e)
        except AttributeError:
            isFile = True
        if isFile:
            self.html = html
        else:
            pass
        self.hypertext = self.html.read()
        
if __name__ == "__main__":
    try:
        infile = open("index.html")
    except IOError as e:
        print ("Exception: ", e)
    html = HTML(infile)
    print (html.hypertext)
    infile.close()
        
