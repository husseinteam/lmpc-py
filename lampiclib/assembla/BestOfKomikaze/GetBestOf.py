# -*- coding: utf8 -*-

from urllib.request import urlopen, urlretrieve
from datetime import date as Date
from datetime import timedelta
#from xml.dom import minidom
from os.path import basename
import re

ONE_DAY = timedelta(1)

def asstring(date):
    m = '0' if date.month < 10 else ''
    d = '0' if date.day < 10 else ''
    return "{0}-{3}{1}-{4}{2}".format(date.year, date.month, date.day, m, d)

def nextday(date):
    return date + ONE_DAY

day = Date(2011,1,1)

while day != Date(2014,1,1):
    url = "http://www.komikaze.net/komikaze/%s" % asstring(day)
    print ("checking url :" + url)
    page = str(urlopen(url).read())
    #http://www.komikaze.net/content/komikaze/7890589_erdil565%20(5).jpg
    token = re.findall('content\/komikaze\/(.+?)\.jpg', page)[0]
    try:
        image = "http://www.komikaze.net/content/komikaze/{0}.jpg".format(token)
        urlretrieve(image, "images//{0}".format(basename(image)))
        print(image + " downloaded")
    except Error as er:
        print('error: {0}'.format(er))
    day = nextday(day)
    


