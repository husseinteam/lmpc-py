import sys, numpy
try:
    import pygame
except ImportError:
    print   ('''
            This Program Needs Pygame module to be installed.
            For windows you can download it from this adress:
                http://pygame.org/ftp/pygame-1.8.1release.win32-py2.5.msi
            For linux this adress will be helpful:
                http://pygame.org/ftp/pygame-1.8.1release.tar.gz
            ''')
    sys.exit(0)
    
pygame.surfarray.use_arraytype("numpy")

MAX_COLORVALUE = 255

def rgb2hsl(rgbcolor):
  r,g,b = [float(color)/float(MAX_COLORVALUE) for color in rgbcolor]
  h,s,l = 0.0,0.0,0.0
  maxcolor = r>g and (r>b and r or b) or (g>b and g or b)
  mincolor = r<g and (r<b and r or b) or (g<b and g or b)
  l = (maxcolor + mincolor)/2
  if maxcolor == mincolor:
    s = h = 0.0
  elif l <= 0.5:
    s = (maxcolor-mincolor)/(maxcolor+mincolor)
  elif l >= 0.5:
    s = (maxcolor-mincolor)/(2.0-maxcolor-mincolor)
  if r == maxcolor:
    h = (g-b)*(maxcolor-mincolor)
  elif g == maxcolor:
    h = 2.0 + (b-r)*(maxcolor-mincolor)
  elif b == maxcolor:
    h = 4.0 + (r-g)*(maxcolor-mincolor)
  h = int(h*60)
  if h < 0:
    h += 360
  return (h,s,l)
  
def horizontal_gradient_fill(surfsize, startcolor, endcolor):
        width, height = surfsize
        sampsurf = pygame.Surface((width,1))
        scol_r, scol_g, scol_b = startcolor
        ecol_r, ecol_g, ecol_b = endcolor
        piece = 1.0/width
        piece_r = (ecol_r - scol_r) * piece
        piece_g = (ecol_g - scol_g) * piece
        piece_b = (ecol_b - scol_b) * piece
        for x in range(width):
                sampsurf.set_at((x,0), (
                                        int(scol_r+piece_r*x),
                                        int(scol_g+piece_g*x),
                                        int(scol_b+piece_b*x)
                                        )
                                )
        return pygame.transform.scale(sampsurf, surfsize)

def alter_brightness(color, qtnt):
    if len(color) == 4:
        r,g,b,a = color
        temp = 0
        if qtnt>=0:
            r,g,b,temp = [int(c+(255-c)*qtnt) for c in color]
            return (r,g,b,a)
        else:
            r,g,b,temp = [int(c-(255-c)*qtnt) for c in color]
            return (r,g,b,a)
    elif len(color) == 3:
        if qtnt>=0:
            return tuple([int(c+(255-c)*qtnt) for c in color])
        else:
            return tuple([int(c-(255-c)*qtnt) for c in color])

def alter_alpha(color, qtnt):
    (r,g,b,a) = color
    a = a+(255-a)*qtnt
    return (r,g,b,a)

if __name__ == '__main__':
  rgbcolor = (34, 127, 78)
  (h,s,l) = rgb2hsl(rgbcolor)
  print ('h:' + str(h) + ' s:' + str(s) + ' l:' + str(l))
  pygame.display.init()
  screen = pygame.display.set_mode((200,50))
  surfsize = (200,50)
  color = (0,0,255)  #blue
  newsurf = pygame.Surface((200,1)).convert()
  qtnt = 1
  add = 1.0/100
  for x in range(100):
      qtnt -= add
      newsurf.set_at((x,0), alter_brightness(color, qtnt))
  for x in range(100, 200):
      qtnt += add
      newsurf.set_at((x,0), alter_brightness(color, qtnt))
  newsurf = pygame.transform.scale(newsurf,(200,50))
  pygame.display.update(screen.blit(newsurf,(0,0)))
  
