import sys, numpy, color
try:
    import pygame
except ImportError:
    print   ('''
            This Program Needs Pygame module to be installed.
            For windows you can download it from this adress:
                http://pygame.org/ftp/pygame-1.8.1release.win32-py2.5.msi
            For linux this adress will be helpful:
                http://pygame.org/ftp/pygame-1.8.1release.tar.gz
            ''')
    sys.exit(0)

pygame.surfarray.use_arraytype("numpy")

class ProgBarArgs:
    def __init__(self, parentsurf, size):
        self.size = size
        self.padding = 5
        self.colors = {}
        self.colors["back"]     = [0,0,0]       #black
        self.colors["init"]     = [255,255,255] #white
        self.colors["start"]    = [255,255,0]   #yellow
        self.colors["end"]      = [255,0,0]     #red
        self.whole = None
        self.step = None
        self.parentsurf = parentsurf
        if parentsurf is not None and size is not None:
            self.rel_coord = [
                parentsurf.get_width()-size[0]-self.padding, \
                parentsurf.get_height()-size[1]-self.padding
                ]
        else:
            self.rel_coord = [5,5]

class ProgressBar:
    def __init__(self, progbarArgs):
        self.size       = progbarArgs.size
        self.pad        = progbarArgs.padding
        self.colors     = progbarArgs.colors
        self.whole      = progbarArgs.whole
        self.step       = progbarArgs.step
        self.parentsurf = progbarArgs.parentsurf
        self.rel_coord  = progbarArgs.rel_coord
        self.surf       = None
        self.screen     = None
        if progbarArgs.parentsurf == None:  #standalone mode
            self.screen = pygame.display.set_mode(self.size)
            self.scren.fill(self.colors["back"])
            self.bar = pygame.Surface((self.size[0]-2*self.pad,self.size[1]-2*self.pad)).convert()
            self.bar.fill(self.colors["init"])
            pygame.display.update(self.screen.blit(self.bar, (5,5)))
        else:       #parentsurf is available
            self.parentsurf = progbarArgs.parentsurf
            parentsize = (self.parentsurf.get_width(), self.parentsurf.get_height())
            if (self.rel_coord[0] + self.size[0] > parentsize[0]) \
                or (self.rel_coord[1] + self.size[1] > parentsize[1]): #overflow check
                print("Incorrect Usage!! Child Surface Overflows Parent!")
                sys.exit(1)
            self.surf = pygame.Surface((self.size[0],self.size[1]))
            self.surf.fill(self.colors["back"])
            self.bar = pygame.Surface((self.size[0]-2*self.pad,self.size[1]-2*self.pad))
            self.bar.fill(self.colors["init"])
            self.surf.blit(self.bar, (self.pad, self.pad))
            pygame.display.update(self.parentsurf.blit(self.surf, (self.rel_coord[0], self.rel_coord[1])))
        if self.whole < self.step :
            print("Error: ProgressBar length can't be smaller than step")
            sys.exit(1)
        self.slice = float(self.step)/float(self.whole)
        self.percent = 0.0

    def showprogress(self):
        if self.percent > 1.0:
            self.percent = 1.0
        if self.percent == 0.0:
            barwidth = 1
        else:
            barwidth = int((self.size[0]-2*self.pad)*self.percent)
        barheight = self.size[1]-2*self.pad
        gradbar = color.horizontal_gradient_fill((barwidth, barheight), self.colors['start'], self.colors['end'])
        self.bar.fill(self.colors["init"])
        self.bar.blit(gradbar, (0,0))
        if self.parentsurf == None:     #standalone mode
            self.screen.fill(self.colors["back"])
            pygame.display.update(self.screen.blit(self.bar, (self.pad,self.pad)))
        else:                           #parentsurf available
            self.surf.fill(self.colors["back"])
            self.surf.blit(self.bar, (self.pad, self.pad))
            pygame.display.update(self.parentsurf.blit(self.surf, (self.rel_coord[0], self.rel_coord[1])))

    def reset(self):
        self.percent = 0.0
        self.showprogress()

    def makestep(self):
        if self.percent < 1.0:
            self.percent += self.slice
            self.showprogress()
        elif self.percent == 1.0:
            self.showprogress()
        else:
            self.reset()

    def isEnd(self):
        if self.percent<1.0:
            return False
        else:
            return True

if __name__ == "__main__":
    topscreen = pygame.display.set_mode((400,300))
    topscreen.fill((255,255,255)) #white
    pygame.display.update()
    progbarArgs = ProgBarArgs(topscreen, (150,25))
    progbarArgs.whole       = 5000
    progbarArgs.step        = 400
    #progbarArgs.rel_coord   = [200,100]
    pb = ProgressBar(progbarArgs)

    while not pb.isEnd():
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                print("exiting.")
            elif e.type == pygame.MOUSEBUTTONDOWN:
                if e.button == 1:
                    pb.makestep()
                    print ("wave constructed")

    print("end.")
