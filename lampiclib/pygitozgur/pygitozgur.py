import urllib.request as ur
import os

jpgurlfrmt = "http://yigitozgur.net/galeri/albums/arsiv/{0:04d}.jpg"
datadirname = "data"
DATADIR = os.path.join(os.getcwd(), datadirname)
FILENAMEFRMT = os.path.join(DATADIR, "{0:04d}.jpg")

if os.path.exists(os.path.join(os.getcwd(), datadirname)):
  print("deleting", datadirname);
  for e in os.listdir(datadirname):
    os.remove(os.path.join(DATADIR, e))
  os.rmdir(datadirname)
os.mkdir(datadirname)
print(datadirname, "recreated")


def pygitozgur():
  for i in range(1500):
    url = None
    lastfn = None
    try:
      lastfn = FILENAMEFRMT.format(i)
      ur.urlretrieve(jpgurlfrmt.format(i), lastfn)
    except:
      print("error with file name:\n\t" + lastfn)
      os.remove(lastfn)
    else:
      print("successfully retrieved file:\n\t" + lastfn)

pygitozgur()
