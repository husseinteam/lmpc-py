function getImageSize(imgwidth, imgheight) {
	var screenW = document.body.clientWidth;
	var screenH = document.body.clientHeight;
	if (typeof screenW == 'undefined') {
		alert("error getting screen dimensions");
		return;
	}
	var coef = 1.0;
	var wratio = screenW / imgwidth;
	var hratio = screenH / imgheight;
	coef = Math.max(wratio, hratio)
	//one quarter of screen:
	//alert('imgwidth=' + imgwidth + ' imgheight=' + imgheight);
	var w = eval(imgwidth+'*'+coef);
	var h = parseInt(imgheight, 10) * coef;
	//alert('w=' + w + ' h=' + h);
	return { width : w, height : h}; 
}

function pad(number, length) { 
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
    return str;
}
function nextImg(id)
{
	var obj = document.getElementById(id);
	var src = obj.src;
	var num = 0;
	var pos = src.indexOf('/data/');
	if (pos > 0) {
		src = src.substr(0, pos + 6);
		num = parseInt(obj.src.substr(src.length), 10);
	}
	else
		alert('invalid pos:' + pos);
	num++;
	obj.src = src + pad(num, 4) + '.jpg';
	var size = getImageSize(obj.width, obj.height);

	//obj.width = size.width;
	//obj.height = size.height;
	return false;
}
function floatTest() {
	alert("3/4=" + (3/4));
	alert("9/4=" + (9/4));
}