from monad import Monad;

class Ring(object):
  def __init__(self, n, direction = 1):
    assert(n > 0);
    self.N = n;
    m = Monad();
    self.Entity = m;
    for i in range(0, n - 1):
      m = m.BindNext(Monad(direction));
    m.BindNext(self.Entity);

class BaseRing(Ring):
  def __init__(self, n):
    Ring.__init__(self, n);


class ComplementRing(Ring):
  def __init__(self, n):
    Ring.__init__(self, n, -1);

class Biring(object):
  def __init__(self, n):
    self.BaseRing = BaseRing(n);
    self.ComplementRing = ComplementRing(n);
    
  def Path(self):
    mb, mc = self.BaseRing.Entity, self.ComplementRing.Entity
    yield mb, mc
    for ib, ic in zip(range(self.BaseRing.N), range(self.ComplementRing.N)):
      mb = mb.Next
      mc = mc.Next
      yield mb, mc;




b = Biring(10);
for mb, mc in b.Path():
  print(mc.Direction)
