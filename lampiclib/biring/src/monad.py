

class Monad(object):
  def __init__(self, direction = 1):
    #1 or -1
    assert(direction in [-1, 1]);
    self.Direction = direction;
    self.Next = None;

  def BindNext(self, m):
    assert(isinstance(m, Monad));
    self.Next = m;
    return m;


m = Monad();
