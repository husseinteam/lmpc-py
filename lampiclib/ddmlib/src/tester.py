'''
Created on 22 Eyl 2010

@author: Ozgur
'''
from core.ddmreader import ddmreader
from output import dumper


def test_digester(path):
    ddm = ddmreader(path)
    ddm.read()
    dumper.dump_list(ddm.digester.tagseq, True)
#    collected = ddm.CollectElementsByClass('"uri url"')
#    dumper.dumpn('number of "uri url" classed tags', len(collected))
#    print ddm.get_text()
