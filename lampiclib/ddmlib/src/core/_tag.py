'''
Created on 22 Eyl 2010

@author: Ozgur
'''

class tag(object):
    '''
    represents a tag
    '''
    def __init__(self):
        '''
        Constructor
        '''
        self.iBegin = -1;
        self.iEnd = -2;
        self.x, self.y, self.alpha, self.beta = None, None, None, None;
        self.Depth = -1;
        self.Index = -1;
        self.Name = '';
        self.Type = '';
        self.Attributes = {};
        
