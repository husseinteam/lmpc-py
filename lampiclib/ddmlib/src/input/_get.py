'''
Created on 21 Eyl 2010

@author: Ozgur
'''

from urllib.error import URLError
from urllib.request import urlopen
import os
import sys

def __tryfileread(path):
    docf = open(path, "r")
    doctext = docf.read()
    docf.close()
    return doctext

def read_document(path):
    '''    
    reads or downloads document   
    '''
    #hypertext string the html file or url gave:    
    if os.path.isfile(path):
        return __tryfileread(path);
    try:        
        return str(urlopen(path).read());
    except URLError as ue:
        print ('errortext : %s value: %s' % (ue, path))
        sys.exit(0)
    except (ValueError):
        print ('unknown url type: %s' % (path))
        sys.exit(0)
    else:
        print('unknown error.')
        sys.exit(0)
