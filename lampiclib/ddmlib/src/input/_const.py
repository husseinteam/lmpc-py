'''
Created on 22 Eyl 2010

@author: Ozgur
'''

class _tid(object):
    def __init__(self):
        self.Start = "<";
        self.End = ">";
        self.Finalizer = "/";
    
    def get_endtag(self, tagname):
        return "%s%s%s%s" % (self.Start, self.Finalizer, tagname, self.End);

class _aid(object):
    def __init__(self):
        self.ValueWrappers = ["\"", "'"];
        self.Assigner = "=";


#sum:
consts = {
          "TagTypes" : {"Start" : "Start", "End" : "End", "Single" : "Single"},
          "Logger" : {"LogFile" : "log.txt", "NewLine" : "\n", "Seperator" : "--------"},
          "Irregulars" : [["![CDATA[", "]]"], ["!--", "--"], ["!DOCTYPE", ""]],
          "Skips" : ["script"],
          "WorkerPrefix" : "_w_",
          "DumpFile" : "file.txt" 
          }
TagIdentifier = _tid();
AttributeIdentifier = _aid();
