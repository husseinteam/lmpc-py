'''
Created on 22 Eyl 2010

@author: Ozgur
'''
import os
import sys

from process import toolbox
from input._const import consts

def dump(formatstr, *p):
    if formatstr.find('%') == -1:
        nameliststr = formatstr
        formatstr = ' : %s '.join([str(n).strip() for n in nameliststr.split(',')]) + ' : %s'
    print (formatstr % (p))

def dumpn(formatstr, *p):
    if formatstr.find('%') == -1:
        nameliststr = formatstr
        formatstr = ' : %s '.join([str(n).strip() for n in nameliststr.split(',')]) + ' : %s'
    if len(p) == 0:
        print(formatstr + "\n")
    else:
        print (formatstr % (p) + "\n");

def dump_tag(t):
    vgen = (var for var in dir(t) if not (hasattr(getattr(t, var), "__call__") or var.startswith("__")));
    for v in vgen:
        print ('variable %s : value is %s\n' % (v, getattr(t, v)));
    print ('='*80 + '\n');

def dump_list(seq, to_disk=False):
    r = ""
    for s in seq:
        if toolbox.is_iterable(s):
            dump_list(s)
        header = ''
        dump = ''
        vList = [(var, max(len(repr(getattr(s, var))), len(var))) \
            for var in dir(s) \
            if (not hasattr(getattr(s, var), "__call__") and not var.startswith("__"))];
        header = ' | '.join([('%s' % (v)).rjust(mj) for v, mj in vList]) + '\n'
        dump = ' | '.join([('%s' % (getattr(s, v))).rjust(mj) for v, mj in vList])
        hr = '=' * max(len(header), len(dump))
        r = r + "\n" + header;
        r = r + "\n" + dump;
        r = r + "\n" + hr;
    if to_disk:
        to_file(r, "w")
    else:
        print(r);

def to_file(text, mode):
    path = os.path.join(os.getcwd(), consts["DumpFile"])
    try:
        f = open(path, mode)
    except:
        print ("error opening %s" % (path));
        sys.exit()
    f.write(text)
    f.flush()
    f.close()
