'''
Created on 22 Eyl 2010
@author: Ozgur
'''

from process._backfiller import backfiller
from process._forthfiller import forthfiller
from core._worker import worker;

class digester(worker):
    def __init__(self, docpath):
        worker.__init__(self, self);
        self.docpath = docpath;
        self.doctext = '';
        self.tagseq = None;

    def digest(self):
        self.run();
    
    def _w_0_fillforth(self):
        ff = forthfiller(self.docpath);
        ff.search();
        self.doctext = ff.doctext;
        self.tagseq = ff.tagseq;

    def _w_1_fillback(self):
        bf = backfiller(self.tagseq);
        bf.run();
        self.tagseq = bf.tagseq
    
if __name__ == "__main__":
    d = digester('http://www.w3.org/');
    d.digest();
            
