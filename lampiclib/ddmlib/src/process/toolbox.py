from input._const import consts

# To change this template, choose Tools | Templates
# and open the template in the editor.

__author__ = "Ozgur"
__date__ = "$23.Eyl.2010 14:28:26$"

    
def is_iterable(obj):
    try:
        iter(obj);
    except:
        return False;
    return True;


def is_border(tag):
    return tag.Type == consts["TagTypes"]["Start"] or  \
        tag.Type == consts["TagTypes"]["End"]


def is_start(tag):
    return tag.Type == consts["TagTypes"]["Start"]

def is_end(tag):
    return tag.Type == consts["TagTypes"]["End"]


def extract_attr(tag):
    if len(tag.Attributes) == 0:
        return '';
    attrstr = '';
    for (n, v) in tag.Attributes.items():
        attrstr = attrstr + '%s=%s ' % (n, v)
    attrstr = attrstr.strip();
    return attrstr;

if __name__ == "__main__":
    print ("Hello World")
