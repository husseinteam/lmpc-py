from process import toolbox
from input._get import read_document;
from input._const import *;
from core._worker import worker;
from core._tag import tag;
# To change this template, choose Tools | Templates
# and open the template in the editor.

__author__ = "Ozgur"
__date__ = "$23.Eyl.2010 13:00:03$"

class forthfiller(worker):
    '''
    digests document
    '''
    def __init__(self, docpath):
        '''
        internalizes document
        '''
        worker.__init__(self, self);
        self.docpath = docpath
        self.doctext = None;
        self.start = -1;
        self.end = None;
        self.eof = False;
        self.t = None;
        self.tagtext = None;
        self.tagseq = [];
        self.index = 0;
        self.x, self.y = 0, 0;

    def search(self):
        self.doctext = read_document(self.docpath)
        self.start = self.__find(TagIdentifier.Start);
        while self.eof == False:
            self.run();
    
    def __extract_attributes(self, eqs_i):
        #eot:end of tag
        eot = False;
        #extract name:
        found_vw = None;
        name_start = self.__rfind(" ", eqs_i);
        value_start = eqs_i + 1;
        for vw in AttributeIdentifier.ValueWrappers:
            if self.doctext[eqs_i + 1] == vw:
                found_vw = vw;
        name = self.doctext[name_start:eqs_i].strip(" ");
        
        #extract value:
        if found_vw == None:
            #attr is like this:  name=value
            #extract value:
            value_end = self.__find(" ", value_start, self.end);
            if value_end == -1:
                #no whitespace:
                value_end = self.__find(TagIdentifier.End, value_start);
                eot = True;
            value = self.doctext[value_start : value_end].strip(" ");
        else:
            #attr : name="value"
            #correct:
            value_end = self.__find(found_vw, value_start + 1);
            assert(value_end != -1);
#            self.__correct_self_end(value_end, found_vw);
            value = self.doctext[value_start : value_end + 1];
            eot = self.__find(AttributeIdentifier.Assigner, value_start, self.end) == -1;
        return {"Name" : name, "Value" : value, "EOT" : eot, "ValueEnd" : value_end};
                           
    def __find(self, str, start=0, end=None):
        if not end:
            end = len(self.doctext);
        for i in range(start, end, 1):
            if self.doctext[i:i + len(str)] == str:
                return i;
        return - 1;
                
    def __find_all(self, str, start=0, end=None):
        indexes = [];
        if end == None:
            end = len(self.doctext);
        for i in range(start, end, 1):
                if self.doctext[i:i + len(str)] == str:
                    indexes.append(i);
        return indexes;
    
    def __rfind(self, str, start):
        for i in range(start, -1, -1):
            if self.doctext[i:i + len(str)] == str:
                return i;
    
    def __correct_self_end(self, value_end, vw):
        value_start = None;
        while value_end > self.end:
            #irregular tagtext: <tagname name='script...x>y'/>
            #correct self.end:
            self.end = self.__find(TagIdentifier.End, value_end + 1)
            #correct value_end:
            # <tagname script="func(x) { if x > 0 return false;} " script2="x>8;">
            value_start = self.__find(vw, value_end + 1);
            #now if a second trap exists like above; value_start<self.end..
            #else value_start>self.end or value_start=-1
            if value_start > self.end or value_start == -1:
                break;
            value_end = self.__find(vw, value_start + 1);
        #self.end corrected.

    def __get_depth_of_single_tag(self):
        seq = self.tagseq
        limit = len(seq) - 1
        for i in range(limit, 0, -1):
            if toolbox.is_border(seq[i]):
                return toolbox.is_start(seq[i]) and \
                    seq[i].Depth + 1 or seq[i].Depth
        return None
    
    #work list:       
    def _w_0_initialize_parse(self):
        self.end = self.__find(TagIdentifier.End, self.start + 1);
        self.t = tag();
        self.tagtext = self.doctext[self.start:self.end + 1];
        assert(self.end != -1);

    def _w_0a_check_for_irregulars(self):
        for ir in consts["Irregulars"]:
            ir_start = TagIdentifier.Start + ir[0]
            ir_end = ir[1] + TagIdentifier.End
            if self.tagtext.startswith(ir_start):
                if ir_end == '':
                    self.end = self.__find(TagIdentifier.End, self.start + 1)
                else:
                    self.end = self.__find(ir_end, self.start + 1)
                return ["_w_1_", "_w_2_", "_w_3_", "_w_4_", "_w_5_"]
    
#    def _w_0b_check_for_skips(self):
#        for sk in consts["Skips"]:
#            #<script> .. </script>
#            sk_start = TagIdentifier.Start + sk
#            sk_end = TagIdentifier.Start + TagIdentifier.Finalizer + sk
#            if self.tagtext.startswith(sk_start):
#                self.end = self.__find(sk_end, self.start + 1) - 1
#                break
#    
    def _w_1_check_for_attr_exists(self):
        omits = None;
        if self.tagtext.find(AttributeIdentifier.Assigner) == -1:
            #non attributed tag: <html>
            #print (self.tagtext)
            omits = ["_w_4_"];
        return omits;

    def _w_2_set_tag_name(self):
        i_end = self.tagtext.find(" ");
        if i_end == -1:
            #<html>
            i_end = self.end;
        tagname = self.tagtext[:i_end].strip()\
            .strip(TagIdentifier.Start)\
            .strip(TagIdentifier.End)\
            .strip(TagIdentifier.Finalizer);
        self.t.Name = tagname;
            
    def _w_3_set_type(self):
        if self.__find(TagIdentifier.get_endtag(self.t.Name)) == -1:
            self.t.Type = consts["TagTypes"]["Single"];
        elif self.tagtext.startswith(TagIdentifier.Start + TagIdentifier.Finalizer):
            self.t.Type = consts["TagTypes"]["End"];
        elif self.__find(TagIdentifier.get_endtag(self.t.Name)) != -1:
            self.t.Type = consts["TagTypes"]["Start"];
        else:
            #dumper.dump_tag(self.t)
            raise Exception
        
    def _w_4_set_attributes(self):
        eqs_index = self.__find(AttributeIdentifier.Assigner, self.start + 1);
        assert(eqs_index != -1);
        eot = False
        
        while eot == False:
            ret_dict = self.__extract_attributes(eqs_index);
            self.t.Attributes[ret_dict["Name"]] = ret_dict["Value"];
            eqs_index = \
                self.__find(AttributeIdentifier.Assigner, ret_dict["ValueEnd"] + 1);
            if eqs_index == -1:
                break;
            eot = ret_dict["EOT"]
            
    def _w_5_forthfill_tagseq(self):
        #fill properties
        self.t.Index = self.index;
        self.t.iBegin = self.start;
        self.t.iEnd = self.end;

        #set self.x and self.y
        if self.t.Type == consts["TagTypes"]["Start"]:
            self.x = self.x + 1;
        elif self.t.Type == consts["TagTypes"]["End"]:
            self.y = self.y + 1;

        #set Depth
        if self.t.Type == consts["TagTypes"]["Start"]:
            self.t.Depth = self.x - self.y - 1
        elif self.t.Type == consts["TagTypes"]["End"]:
            self.t.Depth = self.x - self.y
        elif self.t.Type == consts["TagTypes"]["Single"]:
            self.t.Depth = len(self.tagseq) > 0 and \
                self.__get_depth_of_single_tag() or 0
        else:
            #dumper.dump_tag(self.t)
            raise
        #set self.t.x self.t.y
        self.t.x = self.x;
        self.t.y = self.y;

        self.tagseq.append(self.t);
        
    def _w_6_finalize_parse(self):
        self.index = self.index + 1;
        #update self.start:
        self.start = self.__find(TagIdentifier.Start, self.end + 1);
        #dumper.dump('self.tagtext', self.tagtext)
        self.eof = self.start == -1;

if __name__ == "__main__":
    print ("Hello World")
