# To change this template, choose Tools | Templates
# and open the template in the editor.


__author__ = "Ozgur"
__date__ = "$23.Eyl.2010 11:49:12$"

import tester
import sys, os
sys.path.append("..{0}..{0}walky{0}src".format(os.path.sep))

from walker.MWalker import TWalker


if __name__ == "__main__":
    if len(sys.argv) > 1:
        w = TWalker()
        w.Start();
        tester.test_digester(sys.argv[1]);
        w.StopAndReport();
    else:
        print("no parameters given..\nexecuting with standart params.")
        w = TWalker()
        w.Start();
        tester.test_digester("http://www.w3.org");
        w.StopAndReport();
