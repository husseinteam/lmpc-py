'''
Created on 30 Oca 2011

@author: dr.ozgur.sonmez
'''
from datalayer.MFacade import TFacade;

class FacadeElement(TFacade):
    def __init__(self):
        TFacade.__init__(self);
        
    def InsertElement(self, entityElement):
        self.Init();
        name = entityElement.Name;
        parentID = entityElement.ParentID is None and "NULL" or entityElement.ParentID;
        directory = entityElement.Directory == True and 1 or 0;
        sql = "";
        try:
          sql = "INSERT INTO elements (Name, Directory, ParentID) VALUES ('{0}', {1}, {2});"\
                .format(name, directory, parentID);
          self.Execute(sql);
          entityElement.ID = self.Identity();
        except Exception:
          name = name.encode("utf-8");
          sql = "INSERT INTO elements (Name, Directory, ParentID) VALUES ('{0}', {1}, {2});"\
                .format(name, directory, parentID);
          sql = sql.replace("b'", "").replace("''", "'");
          try:
            self.Execute(sql);
            entityElement.ID = self.Identity();
          except Exception as err2:
            print("Element could not be inserted: {0}. Message is: {1}\nSql: {2}"\
                .format(entityElement.Name, err2, sql));
            return;
        finally:
            self.Close();
#        print ("Element {0} inserted.".format(entityElement.Name));
